package com.dbbundle.db_main;

import java.io.File;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import java.util.Map.Entry;

import org.identityconnectors.common.script.Script;
import org.identityconnectors.common.script.groovy.GroovyScriptExecutorFactory;
import org.identityconnectors.common.security.GuardedString;
import org.identityconnectors.framework.api.APIConfiguration;
import org.identityconnectors.framework.api.ConfigurationProperties;
import org.identityconnectors.framework.api.ConnectorInfo;
import org.identityconnectors.framework.api.ConnectorInfoManager;
import org.identityconnectors.framework.api.ConnectorInfoManagerFactory;
import org.identityconnectors.framework.api.ConnectorKey;
import org.identityconnectors.framework.api.RemoteFrameworkConnectionInfo;
import org.identityconnectors.framework.common.objects.Attribute;
import org.identityconnectors.framework.common.objects.AttributeBuilder;
import org.identityconnectors.framework.common.objects.Name;
import org.identityconnectors.framework.common.objects.ObjectClass;
import org.identityconnectors.framework.common.objects.Uid;
import org.identityconnectors.framework.api.ConfigurationProperty;
import org.identityconnectors.framework.api.ConnectorFacade;
import org.identityconnectors.framework.api.ConnectorFacadeFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scripting.groovy.GroovyScriptEvaluator;

@SpringBootApplication
public class DbMainApplication {

	//private static final String ObjectClass = null;

    public static void main(String[] args) {
		ConnectorInfoManagerFactory fact = ConnectorInfoManagerFactory.getInstance();

        RemoteFrameworkConnectionInfo remoteFrameworkConnectionInfo = new RemoteFrameworkConnectionInfo("localhost", 8759,
                new GuardedString("hello".toCharArray()));
        ConnectorInfoManager manager = fact.getRemoteManager(remoteFrameworkConnectionInfo);
        ConnectorKey key = new ConnectorKey("net.tirasa.connid.bundles.db.scriptedsql", "2.2.6",
                                             "net.tirasa.connid.bundles.db.scriptedsql.ScriptedSQLConnector");
        ConnectorInfo info = manager.findConnectorInfo(key);

        // From the ConnectorInfo object, create the default APIConfiguration.
        APIConfiguration apiConfig = info.createDefaultAPIConfiguration();

        // From the default APIConfiguration, retrieve the ConfigurationProperties.
        ConfigurationProperties properties = apiConfig.getConfigurationProperties();

        // Print out what the properties are (not necessary)
        List<String> propertyNames = properties.getPropertyNames();
        for(String propName : propertyNames) {
            ConfigurationProperty prop = properties.getProperty(propName);
            System.out.println("Property Name: " + prop.getName() + "\tProperty Type: " + prop.getType());
        }

     // Set all of the ConfigurationProperties needed by the connector.
      //  File gScript=new File("scriptedsql/src/main/resources/samples/TestScript.groovy");

      properties.setPropertyValue("quoting","NONE");
      properties.setPropertyValue("host","localhost");
      properties.setPropertyValue("port","3306" );
      properties.setPropertyValue("user","root");
      properties.setPropertyValue("password",new GuardedString("password".toCharArray()));
      properties.setPropertyValue("database","user");
      properties.setPropertyValue("jdbcDriver","com.mysql.jdbc.Driver");
      properties.setPropertyValue("jdbcUrlTemplate","jdbc:mysql://localhost:3306/user");
      // properties.setPropertyValue("autoCommit",true);
      // properties.setPropertyValue("rethrowAllSQLExceptions","");
      // properties.setPropertyValue("nativeTimestamps","");
      // properties.setPropertyValue("allNative","");
      properties.setPropertyValue("validConnectionQuery","");
      //properties.setPropertyValue("datasource","");
      //properties.setPropertyValue("jndiProperties","");
      properties.setPropertyValue("createScriptFileName","D:\\OneDrive\\Desktop\\Cymmetri\\ConnIdDBBundle-db-2.2.6\\ConnIdDBBundle-db-2.2.6\\scriptedsql\\src\\main\\resources\\samples\\CreateScript.groovy" );
      properties.setPropertyValue("testScriptFileName", "D:\\OneDrive\\Desktop\\Cymmetri\\ConnIdDBBundle-db-2.2.6\\ConnIdDBBundle-db-2.2.6\\scriptedsql\\src\\main\\resources\\samples\\TestScript.groovy");
      properties.setPropertyValue("updateScriptFileName", "D:\\OneDrive\\Desktop\\Cymmetri\\ConnIdDBBundle-db-2.2.6\\ConnIdDBBundle-db-2.2.6\\scriptedsql\\src\\main\\resources\\samples\\UpdateScript.groovy");
      properties.setPropertyValue("deleteScriptFileName", "D:\\OneDrive\\Desktop\\Cymmetri\\ConnIdDBBundle-db-2.2.6\\ConnIdDBBundle-db-2.2.6\\scriptedsql\\src\\main\\resources\\samples\\DeleteScript.groovy");





        // Use the ConnectorFacadeFactory's newInstance() method to get a new connector.
        ConnectorFacade conn = ConnectorFacadeFactory.getInstance().newInstance(apiConfig);
		
        // Make sure we have set up the Configuration properly


        conn.validate();
    	conn.test();
        Map<String,Object> mp=new HashMap<>();
        String uid=UUID.randomUUID().toString();
        mp.put(Name.NAME,uid);
        mp.put("firstname", "mohd");
        mp.put("lastname", "musteweafa");
        mp.put("fullname", "eweq");
        mp.put("email", "ma2k5@gmail.com");
        mp.put("organization","cymmetri");

        Set<Attribute> attrib=new HashSet<>();

        for(Entry<String,Object> mEntry: mp.entrySet()){
            attrib.add(AttributeBuilder.build(mEntry.getKey(),mEntry.getValue()));
        }

        conn.update(ObjectClass.ACCOUNT, new Uid("1b92f516-a225-47f0-a92e-33ba2252b44b"),attrib, null);
    //  conn.create(ObjectClass.ACCOUNT, attrib,null);
    //  conn.delete(ObjectClass.ACCOUNT, new Uid("111w"), null);
	}
}
